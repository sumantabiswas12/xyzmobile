package com.hcl.xyzmobile;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.xyzmobile.bo.AdminApproval;
import com.hcl.xyzmobile.bo.EnableResp;
import com.hcl.xyzmobile.bo.NewConnectionReq;
import com.hcl.xyzmobile.entity.NewConnection;

@SpringBootApplication
@RestController
@Configuration
@EnableScheduling
public class XyzmobileApplication {

	public static void main(String[] args) {
		SpringApplication.run(XyzmobileApplication.class, args);
	}

	@Autowired
	private com.hcl.xyzmobile.service.NewConnectionService newConnectionService;
	
	@PostMapping(value="/newconnection", produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Integer createNewConnection(@RequestBody NewConnectionReq req) {
		Integer reqId=null;
		reqId = newConnectionService.createNewConnection(req);
		return reqId;
	}
	
	@GetMapping(value="/track", produces=MediaType.APPLICATION_JSON_VALUE)
	public String track(@RequestParam Integer reqId) {
		return newConnectionService.getStatusofReq(reqId);
	}
	
	@GetMapping(value="/allreq", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Integer> allreq(@RequestParam String user) {
		if("admin".equals(user)) {
			return newConnectionService.getAllReqForadmin();
		}
		return null;
	}
	
	@GetMapping(value="/allreqdetls", produces=MediaType.APPLICATION_JSON_VALUE)
	public NewConnection allreqDetls(@RequestParam String user, @RequestParam Integer reqId) {
		if("admin".equals(user)) {
			return newConnectionService.getDetailsOfReq(reqId);
		}
		return null;
	}
	
	@PostMapping(value="/changestatus", produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public AdminApproval approveOrReject(@RequestBody AdminApproval admn) {
		return newConnectionService.approvaOrRejectReq(admn);
	}
	/*
	@GetMapping(value="/assignnewno", produces=MediaType.APPLICATION_JSON_VALUE)
	public NewConnection assignnewno(@RequestParam String user, @RequestParam Integer reqId) {
		if("admin".equals(user)) {
			return newConnectionService.assignNew(reqId);
		}
		return null;
	}
	*/
	
	@Scheduled(fixedRate = 5000*60)
	public List<EnableResp> sendActivation() {
		List<EnableResp> respList = newConnectionService.senActivationCode();
		System.out.println("Activation code sent");
		return respList;
	}
	
	
	@GetMapping("/health")
	public String health() {
		return "OK";
	}
}
