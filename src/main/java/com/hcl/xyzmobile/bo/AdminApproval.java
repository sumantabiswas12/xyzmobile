package com.hcl.xyzmobile.bo;

import org.springframework.stereotype.Component;


@Component
public class AdminApproval {
	private String name;
	private String status;
	private String comments;
	private Integer reqId;
	private Integer newMobileNo;
	
	public Integer getNewMobileNo() {
		return newMobileNo;
	}
	public void setNewMobileNo(Integer newMobileNo) {
		this.newMobileNo = newMobileNo;
	}
	public Integer getReqId() {
		return reqId;
	}
	public void setReqId(Integer reqId) {
		this.reqId = reqId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
