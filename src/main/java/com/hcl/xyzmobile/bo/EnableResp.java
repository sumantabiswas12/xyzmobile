package com.hcl.xyzmobile.bo;

import org.springframework.stereotype.Component;


@Component
public class EnableResp {
	private String status;
	private Integer reqId;
	private Integer newMobileNo;
	private Integer activationCode;
	
	public Integer getNewMobileNo() {
		return newMobileNo;
	}
	public void setNewMobileNo(Integer newMobileNo) {
		this.newMobileNo = newMobileNo;
	}
	public Integer getReqId() {
		return reqId;
	}
	public void setReqId(Integer reqId) {
		this.reqId = reqId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(Integer activationCode) {
		this.activationCode = activationCode;
	}
	
}
