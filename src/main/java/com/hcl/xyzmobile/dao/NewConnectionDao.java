package com.hcl.xyzmobile.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hcl.xyzmobile.entity.NewConnection;
import com.hcl.xyzmobile.model.ConnectionRepository;

@Component
public class NewConnectionDao {

	@Autowired
	private ConnectionRepository repo;
	public int saveNewConn(NewConnection newConnection) {

		NewConnection ret = repo.save(newConnection);
		return ret.getReqid();
	}
	public Optional<NewConnection> getDetailsById(Integer reqId) {
		Optional<NewConnection> ret = repo.findById(reqId);
		return ret;
	}
	public Iterable<NewConnection> getAllRequests() {
		return repo.findAll();
	}
	public NewConnection updatereq(NewConnection newConnection) {
		NewConnection ret = repo.save(newConnection);
		return ret;
	}
	public List<Integer> getListofUsedNos() {
		return repo.findMobileNos();
	}
	public List<NewConnection> updateAllApprovedReq(List<NewConnection> updateStatus) {
		return repo.saveAll(updateStatus);	
	}

}
