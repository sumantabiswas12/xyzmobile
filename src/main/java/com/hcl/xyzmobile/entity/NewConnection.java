package com.hcl.xyzmobile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NewConnection")
public class NewConnection {
	@Id
	@Column(name="Reqid")
	private Integer reqid;
	@Column(name="Name")
	private String name;
	@Column(name="Address")
	private String address;
	@Column(name="Phone")
	private Integer phone;
	@Column(name="Doc")
	private String doc;
	@Column(name="Email")
	private String email;
	@Column(name="Plan")
	private String plan;
	@Column(name="Status")
	private String status;
	@Column(name="Comments")
	private String comments;
	@Column(name="Newmobileno")
	private Integer newmobileno;

	
	public Integer getNewmobileno() {
		return newmobileno;
	}
	public void setNewmobileno(Integer newmobileno) {
		this.newmobileno = newmobileno;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getPhone() {
		return phone;
	}
	public void setPhone(Integer phone) {
		this.phone = phone;
	}
	public String getDoc() {
		return doc;
	}
	public void setDoc(String doc) {
		this.doc = doc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public Integer getReqid() {
		return reqid;
	}
	public void setReqid(Integer reqid) {
		this.reqid = reqid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
