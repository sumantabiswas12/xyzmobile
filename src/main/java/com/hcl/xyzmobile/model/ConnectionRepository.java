package com.hcl.xyzmobile.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.xyzmobile.entity.NewConnection;

@Repository
public interface ConnectionRepository extends JpaRepository<NewConnection, Integer> {
	
	

	@Query(value = "SELECT newmobileno FROM NewConnection", nativeQuery = true)
	public List<Integer> findMobileNos();

}