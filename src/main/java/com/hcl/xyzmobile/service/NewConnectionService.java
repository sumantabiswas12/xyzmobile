package com.hcl.xyzmobile.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.xyzmobile.bo.AdminApproval;
import com.hcl.xyzmobile.bo.EnableResp;
import com.hcl.xyzmobile.bo.NewConnectionReq;
import com.hcl.xyzmobile.dao.NewConnectionDao;
import com.hcl.xyzmobile.entity.NewConnection;

@Service
public class NewConnectionService {

	@Autowired
	NewConnectionDao dao;

	public Integer createNewConnection(NewConnectionReq req) {
		NewConnection newConnection = new NewConnection();
		newConnection.setAddress(req.getAddress());
		newConnection.setDoc(req.getDoc());
		newConnection.setEmail(req.getEmail());
		newConnection.setName(req.getName());
		newConnection.setPhone(req.getPhone());
		newConnection.setPlan(req.getPlan());
		newConnection.setReqid((int) (System.currentTimeMillis()));
		newConnection.setStatus("In Progress");

		int id = dao.saveNewConn(newConnection);

		return id > 0 ? newConnection.getReqid() : null;
	}

	public String getStatusofReq(Integer reqId) {
		String status = null;
		Optional<NewConnection> ret = dao.getDetailsById(reqId);
		if (ret.isPresent()) {
			status = ret.get().getStatus();
		} else {
			status = "No request found with the Id";
		}
		return status;
	}

	public List<Integer> getAllReqForadmin() {
		List<Integer> listOfReqId = new ArrayList<>();
		List<NewConnection> listConn = (List<NewConnection>) dao.getAllRequests();
		listOfReqId = listConn.stream().map(x -> x.getReqid()).collect(Collectors.toList());
		return listOfReqId;
	}

	public NewConnection getDetailsOfReq(Integer reqId) {
		Optional<NewConnection> ret = dao.getDetailsById(reqId);
		if (ret.isPresent()) {
			return ret.get();
		}
		return null;
	}

	public AdminApproval approvaOrRejectReq(AdminApproval admn) {
		AdminApproval resp = new AdminApproval();
		NewConnection newConnection = new NewConnection();
		newConnection.setComments(admn.getComments());
		newConnection.setStatus(admn.getStatus());
		newConnection.setReqid(admn.getReqId());

		if ("Approved".equals(admn.getStatus())) {

			List<Integer> usedNos = dao.getListofUsedNos();

			int newmb = generateMobileno();
			if (!usedNos.contains(newmb)) {
				newConnection.setNewmobileno(newmb);
			}
		}

		NewConnection ret = dao.updatereq(newConnection);
		if (ret != null) {
			resp.setReqId(ret.getReqid());
			resp.setName(ret.getName());
			resp.setStatus(ret.getComments());
			resp.setComments(ret.getComments());
			resp.setNewMobileNo(ret.getNewmobileno());
		}
		return resp;
	}

	private int generateMobileno() {
		Random rnd = new Random();
		int number = rnd.nextInt(999999);
		String newmobileno = "9876" + Integer.toString(number);
		return Integer.parseInt(newmobileno);

	}

	public List<EnableResp> senActivationCode() {
		List<EnableResp> enableRespList = new ArrayList<>();
		List<NewConnection> listConn = (List<NewConnection>) dao.getAllRequests();
		List<NewConnection> updateStatus = new ArrayList<>();
		for (NewConnection conn : listConn) {
			NewConnection nw = new NewConnection();
			if ("Approved".equals(conn.getStatus())) {
				nw.setReqid(conn.getReqid());
				nw.setComments(null);
				nw.setStatus("Connection Enabled");
				updateStatus.add(nw);
			}
		}
		List<NewConnection> updateddata = dao.updateAllApprovedReq(updateStatus);

		enableRespList = updateddata.stream().map(x -> {
			EnableResp enableResp = new EnableResp();
			Random rnd = new Random();
			enableResp.setActivationCode(rnd.nextInt(9999));
			enableResp.setNewMobileNo(x.getNewmobileno());
			enableResp.setReqId(x.getReqid());
			enableResp.setStatus(x.getStatus());
			return enableResp;
		}).collect(Collectors.toList());
		
		return enableRespList;
	}

}
